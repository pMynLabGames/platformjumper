﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MaterialColorChanger : MonoBehaviour
{
    public Color Color;

    void Update()
    {
        if (!Application.isPlaying)
        {
            gameObject.GetComponent<Renderer>().material.color = Color;
        }
    }
}
