﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIGameController : MonoBehaviour
{
    public static UIGameController Instance { get; private set; }

    public GameObject gameOverModal;
    public GameObject gameLoosModal;

    void Start()
    {
        Instance = this;
    }
    
    public void ShowGameOverDialog()
    {
        gameOverModal.SetActive(true);
    }

    public void ShowGameLooseDialog()
    {
        gameLoosModal.SetActive(true);
    }

    public void MainMenuClick()
    {
        UIGameController.Instance.ShowGameOverDialog();
    }

    public void NewGameClick()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
