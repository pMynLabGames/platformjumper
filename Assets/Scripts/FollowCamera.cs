﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class FollowCamera : MonoBehaviour
{
    public Transform lookAt;
    public Transform moveTo;
    public Transform rollTo;

    public float chaseTime = .5f;
    public float rollTime = .5f;

    Vector3 roationVel;
    Vector3 moveVel;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

#if UNITY_EDITOR
    void Update()
    {
        if (!Application.isPlaying)
        {
                transform.position = moveTo.position;
                transform.LookAt(lookAt, rollTo.up);
        }
    }
#endif

    void LateUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, moveTo.position, ref moveVel, chaseTime);
        transform.LookAt(lookAt, Vector3.SmoothDamp(transform.up, rollTo.up, ref roationVel, rollTime));
    }
}
