﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerProxy : MonoBehaviour
{
    public GameObject[] FireTriggers;


    private void OnTriggerEnter(Collider other)
    {
        foreach (var trigger in FireTriggers)
        {
            if (trigger.GetComponent<ITrigger>() != null)
            {
                trigger.GetComponent<ITrigger>().Trigger(); ;
            }
        }
    }
}
