﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovablePlatform : MonoBehaviour
{
    public float speedMin = 2f;
    public float speedMax = 5f;

    public Vector3 direction = new Vector3(1,0,0);

    public float offsetMin = 2f;
    public float offsetMax = 5f;

    private float speed = 3;
    private float offset = 3;

    Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        this.startPos = transform.position;
        this.speed = Random.Range(speedMin, speedMax);
        this.offset = Random.Range(offsetMin, offsetMax);
    }

    // Update is called once per frame
    void Update()
    {
        var pos = startPos;
        pos += direction*  this.offset * Mathf.Sin(Time.time * speed)  ;
        transform.position = pos;
    }
}
