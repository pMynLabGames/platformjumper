﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTargetTrigger: MonoBehaviour, ITrigger
{


    public void Trigger()
    {
        UIGameController.Instance.ShowGameOverDialog();
    }
}
