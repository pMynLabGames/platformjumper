﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour, ITrigger
{
    public float openingDoorTime = 2f;
    public float openingDoorOffset = 300f;
    private float elpasedTime=0f;

    public Vector3 openingDoorAxis = Vector3.right;

    bool openingDoor = false;
    Vector3 startPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (openingDoor)
        {
            elpasedTime += Time.deltaTime;
            transform.position = startPos + openingDoorAxis * openingDoorOffset * elpasedTime / openingDoorTime;
            if(elpasedTime > openingDoorTime)
            {
                openingDoor = false;
            }
        }
    }

    public void Trigger()
    {
        openingDoor = true;
        elpasedTime = 0f;
        startPos = transform.position;

        Debug.Log("Opening Door");
    }
}
