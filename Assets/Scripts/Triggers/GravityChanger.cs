﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityChanger : MonoBehaviour
{
    public Vector3 Gravity = Vector3.zero;
    public Transform updateRotation;
    public float changeRotationTime = 1f;
    float elapsedTime = 0f;
    void Update()
    {
        if (updateRotation != null)
        {
            if (elapsedTime <= this.changeRotationTime)
            {
                this.elapsedTime += Time.deltaTime;
                var duration = this.elapsedTime / changeRotationTime;

                var targetRotation = Quaternion.FromToRotation(Vector3.up, -Physics.gravity);
                updateRotation.transform.rotation = Quaternion.Lerp(updateRotation.rotation, targetRotation, duration);
            }
        }
    }
    public void ChangeGravity(Transform transform)
    {
        Physics.gravity = Gravity;
        updateRotation = transform;
    }
}
