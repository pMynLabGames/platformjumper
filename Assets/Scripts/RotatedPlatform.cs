﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatedPlatform : MonoBehaviour
{
    public float rotSpeedZ = 0f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * 2 * Mathf.Sin(Time.time * rotSpeedZ) );

    }

  
}
