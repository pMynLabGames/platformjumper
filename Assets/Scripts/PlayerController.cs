﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public float playerSpeed = .1f;
    public float rotationSpeed = 360;
    public float jumpForce = 10;

    public bool canLookAround = false;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(Physics.gravity);
        Physics.gravity = new Vector3(0, -9.8f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        var foward = this.transform.forward;
        var right = this.transform.right;
        var up = this.transform.up;
        var pos = transform.position;

        if (this.transform.parent != null)
        {
            foward = this.transform.parent.transform.forward;
            right = this.transform.parent.transform.right;
            up = this.transform.parent.transform.up;
        }


        if (Input.GetKey(KeyCode.UpArrow))
        {
            pos += foward * playerSpeed;
            transform.position = pos;
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pos -= right * playerSpeed;
            transform.position = pos;
            if (canLookAround)
                transform.Rotate(-Vector3.up * Time.deltaTime * rotationSpeed);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            pos += right * playerSpeed;
            transform.position = pos;
            if(canLookAround)
            transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed);

        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody>().AddForce( up * jumpForce, ForceMode.Impulse);
        }

        if (IsFallingToLong())
        {
            UIGameController.Instance.ShowGameLooseDialog();
        }
        
    }

    bool IsFallingToLong()
    {
        return transform.position.y < -40;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Platform")){

            if (collision.gameObject.GetComponent<MovablePlatform>() != null)
            {
                transform.parent = collision.gameObject.transform;
            }
        }else if (collision.gameObject.CompareTag("Obstacle"))
        {
            UIGameController.Instance.ShowGameLooseDialog();
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Platform")){
            transform.parent = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<ITrigger>() != null)
        {
            other.GetComponent<ITrigger>().Trigger();
        }
        if(other.GetComponent<GravityChanger>() != null)
        {
            other.GetComponent<GravityChanger>().ChangeGravity(transform);
            canLookAround = true;
        }
        
        Debug.Log("Trigger Enter");

    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Trigger Exit");
       
    }
}
